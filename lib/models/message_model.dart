import 'package:chat_ui_app/models/user_model.dart';

class Message {
  final User sender;
  final String time; // usado frecuentemente para guardar el datetime de apps en produccion
  final String text;
  final bool isLiked;
  final bool unread;

  Message({
    this.sender,
    this.time,
    this.text,
    this.isLiked,
    this.unread,
  });
}

// Tu, el usuarioActivo
final User currentUser = User(
  id: 0,
  name: "Tú",
  imageUrl: 'assets/images/usuario_1.jpg'
);

// Otros Usuarios
final User moises = User(
  id: 1,
  name: "Moises",
  imageUrl: 'assets/images/usuario_2.jpg'
);

final User rossangel = User(
  id: 2,
  name: "Rossangel",
  imageUrl: 'assets/images/usuario_3.jpg'
);

final User mariangel = User(
  id: 3,
  name: "Mariangel",
  imageUrl: 'assets/images/usuario_4.jpg'
);

final User samuel = User(
  id: 4,
  name: "Samuel",
  imageUrl: 'assets/images/usuario_5.jpg'
);

final User anamontse = User(
  id: 5,
  name: "Ana Montserrat",
  imageUrl: 'assets/images/usuario_6.jpg'
);

final User greg = User(
  id: 1,
  name: 'Greg',
  imageUrl: 'assets/images/usuario_1.png',
);

final User james = User(
  id: 2,
  name: 'James',
  imageUrl: 'assets/images/usuario_6.jpg',
);

final User john = User(
  id: 3,
  name: 'John',
  imageUrl: 'assets/images/usuario_2.jpg',
);

final User olivia = User(
  id: 4,
  name: 'Olivia',
  imageUrl: 'assets/images/usuario_5.jpg',
);

final User sam = User(
  id: 5,
  name: 'Sam',
  imageUrl: 'assets/images/usuario_4.jpg',
);

final User sophia = User(
  id: 6,
  name: 'Sophia',
  imageUrl: 'assets/images/usuario_6.jpg',
);

final User steven = User(
  id: 7,
  name: 'Steven',
  imageUrl: 'assets/images/usuario_2.jpg',
);

// Contactos Favoritos
List<User> favoritos = [moises, rossangel, mariangel, samuel, anamontse];

// mensajes de ejemplo
List<Message> messages = [
  Message(
    sender: moises,
    time: '5:30 PM',
    text: 'Saludos.! como estas?',
    isLiked: false,
    unread: true,
  ),

  Message(
    sender: currentUser,
    time: '4:12 PM',
    text: 'Hola Papi como estas?',
    isLiked: false,
    unread: true,
  ),

  Message(
    sender: moises,
    time: '1:30 AM',
    text: 'estas ahi?',
    isLiked: false,
    unread: true,
  ),

  Message(
    sender: moises,
    time: '5:30 PM',
    text: 'Saludos.! como estas?',
    isLiked: false,
    unread: true,
  ),

  Message(
    sender: moises,
    time: '4:12 PM',
    text: 'Hola Papi como estas?',
    isLiked: false,
    unread: true,
  ),

  Message(
    sender: currentUser,
    time: '1:30 AM',
    text: 'estas ahi?',
    isLiked: false,
    unread: true,
  ),
];

// mensajes de ejemplo
List<Message> chats = [
  Message(
    sender: anamontse,
    time: '10:13 PM',
    text: 'Divertido..! Aqui hace mucho Frio',
    isLiked: false,
    unread: true,
  ),

  Message(
    sender: samuel,
    time: '2:19 PM',
    text: 'Acabo de presentar un Examen peluisimo',
    isLiked: false,
    unread: true,
  ),

  Message(
    sender: moises,
    time: '7:30 PM',
    text: 'cuando vienes?',
    isLiked: false,
    unread: true,
  ),

  Message(
    sender: james,
    time: '5:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: olivia,
    time: '4:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: john,
    time: '3:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: sophia,
    time: '2:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: steven,
    time: '1:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: sam,
    time: '12:30 PM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: greg,
    time: '11:30 AM',
    text: 'Hey, how\'s it going? What did you do today?',
    isLiked: false,
    unread: false,
  ),
];
