class User {
  // Declaro las variables
  final int id;
  final String name;
  final String imageUrl;

  // instancio las variables
  User({
    this.id,
    this.name,
    this.imageUrl,
  });
}