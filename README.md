Una Interfaz de un Chat hecha en Flutter y que mustra ademas los mensajes
recibidos por un usuario y las respuestas del receptor. Es solo el UI.

Aquí la Imagen de como quedó:

![Pantalla Inicial](assets/images/Screenshot_2019-11-15-19-27-36.png)

# chat_ui_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
